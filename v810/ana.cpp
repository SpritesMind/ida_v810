/*
 *  Interactive disassembler (IDA).
 *  NEC V810 module
 */

#include "v810.hpp"


bool is_vboy = 0;

//----------------------------------------------------------------------

static char bcond[] = {
  V810_BV,  V810_BL,  V810_BZ,  V810_BNH,
  V810_BN,  V810_BR,  V810_BLT, V810_BLE, 
  V810_BNV, V810_BNC, V810_BNZ, V810_BH,  
  V810_BP,  V810_NOP, V810_BGE, V810_BGT};

#define FORMAT5_REG1(x) ((x) & 0x1f)
#define FORMAT5_REG2(x) (((x) >> 5) & 0x1f)
#define OPCODE(x) ((x) >> 10)

int find_movhi(int reg) {
  int i, newea = prevaddr(cmd.ea);
  for (i=0; i<20; i++) {
    int hword = get_word(newea);
    if (OPCODE(hword) == 0x2f) {
      if (FORMAT5_REG1(hword) == rR0 && FORMAT5_REG2(hword) == reg) return newea;
    }
    newea = prevaddr(newea);
  }
  return BADADDR;
}

/* Look back in disassembly for a ld.w statement, such as:
   ld.w    8C3BFB8h(r28), r28
   (the fact that the offset is a dword means we already went back and patched
   the instruction -- however, we don't have access to its cmd structure,
   so we must recognize the byte pattern which would become the above, 
   then run ua_ana0, then proceed.)
   Returns the immediate offset (i.e. 8C3BFB8h)  */

int find_ldw(int reg) {
  insn_t saved = cmd;
  int i, newea = prevaddr(cmd.ea), retval;
  for(i=0; i<20; i++) {
    int hword = get_word(newea);
    if(OPCODE(hword) == 0x33)
	{
		if (FORMAT5_REG1(hword) == reg && FORMAT5_REG2(hword) == reg) {
			if(decode_insn(newea) != 4) {  
				msg("ERROR: find_ldw unable to parse ld.w at %x\n", newea);
				cmd = saved;
				return BADADDR;
			}

			retval = cmd.Op1.addr;
			cmd = saved;
			return retval;
		}
    }
    newea = prevaddr(newea);
  }
  cmd = saved;
  return BADADDR;
}

void postprocess(void) {
  unsigned int movhi_addr;
  unsigned int new_offset;
  unsigned int jmp_target;

  // msg("Opcode @ %x: %x\n", cmd.ea, opcode);
  switch(cmd.itype) {
	  //TODO add #0xFFFFFFFF,sp -> add #-1, sp
    case V810_MOVEA: // try to detect an offset put on a regster
	//movehi offset's MSB in regX
	//movea  offest's LSB in regX
      if (cmd.Op2.reg != cmd.Op3.reg)
        break; //we can only do our magic if we haven't to spy X registers
      movhi_addr = find_movhi(cmd.Op2.reg); //find a movehi to the same register
      if (movhi_addr != BADADDR) {
        new_offset = cmd.Op1.value & 0xffff;
        cmd.Op1.type = o_near;
        cmd.Op1.dtyp = dt_dword;
        cmd.Op1.addr = new_offset | (((unsigned int)get_word(movhi_addr+2)) << 16);
        if(new_offset & 0x8000) //is negative
          cmd.Op1.addr -= 0x10000;
      }
      /*if (cmd.Op2.reg == rR31) { // special case for lr -- always a  code ref.
        ea_t target = cmd.Op1.value;
        cmd.Op1.type = o_near;
        cmd.Op1.addr = target;
        } */
      break;
    case V810_LD_B: // ld.b
    case V810_LD_H: // ld.h
    case V810_LD_W: // ld.w
      movhi_addr = find_movhi(cmd.Op1.phrase);
      if (movhi_addr != BADADDR) {
        new_offset = cmd.Op1.addr & 0xffff;
        cmd.Op1.type = o_near;
        cmd.Op1.dtyp = dt_dword;
        cmd.Op1.addr = new_offset | (((unsigned int)get_word(movhi_addr+2)) << 16);
        if(new_offset & 0x8000) 
          cmd.Op1.addr -= 0x10000;
      }
      break;
    case V810_JMP:
	  //try to find a previous direct regiter set (LD.W) with was already dword patch (see previous case)
      jmp_target = find_ldw(cmd.Op1.reg);
      if (jmp_target != BADADDR && (get_long(jmp_target) != 0)) {
        cmd.Op1.addr = get_long(jmp_target);
        cmd.Op1.dtyp = dt_dword;
        cmd.Op1.type = o_near;
      }
      break;
  }
}

//----------------------------------------------------------------------
int idaapi ana( void ) {
  
  cmd.Op1.dtyp = dt_byte;
  cmd.Op2.dtyp = dt_byte;

  uint hibyte = ua_next_byte();
  uint lobyte = ua_next_byte();
  uint op_hword = hibyte + (lobyte << 8);
  uint op_word ;
  char format=0;
  if (op_hword == 0) return 0;
  uint opcode = (op_hword >> 10) % 64;

  char offc;
  uint tmp;
  int stmp; // signed
  
  switch(opcode) {
	  case 0:  format=1; cmd.itype=V810_MOV; break;
	  case 16: format=2; cmd.itype=V810_MOV; break;
	  case 1:  format=1; cmd.itype=V810_ADD; break;
	  case 17: format=2; cmd.itype=V810_ADD; break;
	  case 2:  format=1; cmd.itype=V810_SUB; break;
	  case 3:  format=1; cmd.itype=V810_CMP; break;
	  case 19: format=2; cmd.itype=V810_CMP; break;
	  case 4:  format=1; cmd.itype=V810_SHL; break;
	  case 20: format=2; cmd.itype=V810_SHL; break;
	  case 5:  format=1; cmd.itype=V810_SHR; break;
	  case 21: format=2; cmd.itype=V810_SHR; break;
	  case 6:  format=1; cmd.itype=V810_JMP; break;
	  case 7:  format=1; cmd.itype=V810_SAR; break;
	  case 23: format=2; cmd.itype=V810_SAR; break; 
	  case 8:  format=1; cmd.itype=V810_MUL; break;
	  case 9:  format=1; cmd.itype=V810_DIV; break;
	  case 10: format=1; cmd.itype=V810_MULU; break;
	  case 11: format=1; cmd.itype=V810_DIVU; break;
	  case 12: format=1; cmd.itype=V810_OR; break;
	  case 13: format=1; cmd.itype=V810_AND; break;
	  case 14: format=1; cmd.itype=V810_XOR; break;
	  case 15: format=1; cmd.itype=V810_NOT; break;
	  case 18: format=2; cmd.itype=V810_SETF; break;
	  case 22: 
			if (is_vboy == 0)	return 0;
			format=2; 
			cmd.itype=V810_CLI; 
			break;
	  case 24: format=2; cmd.itype=V810_TRAP; break;
	  case 25: format=9; cmd.itype=V810_RETI; break;
	  case 26: format=9; cmd.itype=V810_HALT; break;
	  case 27: return 0;
	  case 28: format=2; cmd.itype=V810_LDSR; break;
	  case 29: format=2; cmd.itype=V810_STSR; break;
	  case 30: 
			if (is_vboy == 0)	return 0;
			format=2; 
			cmd.itype=V810_SEI; 
			break;
	  case 31: format=2; break; //bitString 
	  case 32: case 33: case 34: case 35:
	  case 36: case 37: case 38: case 39:
		format=3;
		cmd.itype = bcond[(op_hword>>9)&0xF];
		break;
	  case 40: format=5; cmd.itype=V810_MOVEA; break;
	  case 41: format=5; cmd.itype=V810_ADDI; break;
	  case 42: format=4; cmd.itype=V810_JR; break;
	  case 43: format=4; cmd.itype=V810_JAL; break;
	  case 44: format=5; cmd.itype=V810_ORI; break;
	  case 45: format=5; cmd.itype=V810_ANDI; break;
	  case 46: format=5; cmd.itype=V810_XORI; break;
	  case 47: format=5; cmd.itype=V810_MOVHI; break;
	  case 48: format=6; cmd.itype=V810_LD_B; break;
	  case 49: format=6; cmd.itype=V810_LD_H; break;
	  case 50: return 0;
	  case 51: format=6; cmd.itype=V810_LD_W; break;
	  case 52: format=6; cmd.itype=V810_ST_B; break;
	  case 53: format=6; cmd.itype=V810_ST_H; break;
	  case 54: return 0;
	  case 55: format=6; cmd.itype=V810_ST_W; break;
	  case 56: format=6; cmd.itype=V810_IN_B; break;
	  case 57: format=6; cmd.itype=V810_IN_H; break;
	  case 58: format=6; cmd.itype=V810_CAXI; break;
	  case 59: format=6; cmd.itype=V810_IN_W; break;
	  case 60: format=6; cmd.itype=V810_OUT_B; break;
	  case 61: format=6; cmd.itype=V810_OUT_H; break;
	  case 62: format=7;  break; // "special"
	  case 63: format=6; cmd.itype=V810_OUT_W; break;
	  default: 
		  cmd.itype=V810_null;
		  msg("Unknown opcode %d (0x%x)", opcode, opcode);
  }

  //get operands according format and opcode
  switch(format) {
	  case 0: msg("Unknown format!"); break;
	  case 1:   // opcode: 15-10 / reg2: 9-5 / reg1: 4-0 
		cmd.Op1.type = o_reg;
		cmd.Op1.reg = (op_hword & 0x1f) + rR0;
	    
		cmd.Op2.type = o_reg;
		cmd.Op2.reg = ((op_hword>>5)  & 0x1f) + rR0;
	    
		if(cmd.itype == V810_JMP) {
		  cmd.Op2.type=o_void; //no op2
		}
		if(cmd.itype == V810_LDSR || cmd.itype == V810_STSR) {
		  cmd.Op1.reg += 32; //system register
		}
		break;
	  case 2:   // opcode: 15-10 / reg2: 9-5 / imm5: 4-0
		if(cmd.itype == V810_SEI || cmd.itype == V810_CLI) {
			//no op
			break;
		}
		if (opcode == 31) //bit string
		{
			switch(op_word &0x1F) {
			  case 0x00: cmd.itype = V810_SCH0BSU; break; 
			  case 0x01: cmd.itype = V810_SCH0BSD; break;
			  case 0x02: cmd.itype = V810_SCH1BSU; break;
			  case 0x03: cmd.itype = V810_SCH1BSD; break;
			  case 0x08: cmd.itype = V810_ORBSU; break;
			  case 0x09: cmd.itype = V810_ANDBSU; break;
			  case 0x0A: cmd.itype = V810_XORBSU; break;
			  case 0x0B: cmd.itype = V810_MOVBSU; break;
			  case 0x0C: cmd.itype = V810_ORNBSU; break;
			  case 0x0D: cmd.itype = V810_ANDNBSU; break;
			  case 0x0E: cmd.itype = V810_XORNBSU; break;
			  case 0x0F: cmd.itype = V810_NOTBSU; break;
			  default:
				msg("%x:Unknown special bit string sub code %x (num=%x)\n", cmd.ea, op_word, op_word &0x1F);
				return 0;
			}
			
			//no operand
		}
		else
		{
			offc = (op_hword & 0x1f);
			if (op_hword & 0x10) offc |= 0xF0;
			cmd.Op1.type = o_imm;
			cmd.Op1.value = offc;
		    
			cmd.Op2.type = o_reg;
			cmd.Op2.reg = ((op_hword>>5)  & 0x1f) + rR0;
		}
		break;
	  case 3:   // opcode: 15-13 / cond: 12-9 / disp9: 8-1 / sub-opcode: 0
		offc=char((op_hword>>1) & 0xff);
		cmd.Op1.addr = cmd.ea + (offc*2);  // signed addition
		cmd.Op1.type = o_near;
		cmd.Op1.dtyp = dt_dword;

		if (cmd.itype == V810_NOP)
			cmd.Op1.type = o_void; //no op for nop

		break;
	  case 4: // opcode: 15-10 / disp25: 9-0, 31-16
		stmp=(op_hword & 0x3ff) << 22;
		stmp /= 64;
		stmp |= ua_next_byte();
		stmp |= ua_next_byte() << 8;
		cmd.Op1.addr = cmd.ip + stmp;
		cmd.Op1.dtyp = dt_dword;
		cmd.Op1.type = o_near;
		break;
	  case 5: // opcode: 15-10 / reg2: 9-5 / reg1: 4-0 / imm16: 31-16
		tmp = ua_next_byte();
		tmp |= ua_next_byte() << 8;
		//		msg("cmd.Op1.value = %x\n", cmd.Op1.value);
		cmd.Op1.value = tmp;
		cmd.Op1.dtyp = dt_dword;
		cmd.Op1.type = o_imm;
		//  op_offset(cmd.ea, 0, REF_OFF32, tmp, tmp & 0xFFFF0000);
	    
		cmd.Op2.type = o_reg;
		cmd.Op2.reg = (op_hword & 0x1f) + rR0;
		cmd.Op3.type = o_reg;
		cmd.Op3.reg = ((op_hword>>5) & 0x1f) + rR0;
		//			if (cmd.itype==V810_MOVEA) 
		//				msg("ana.cpp@%x: %x\n", cmd.ea, cmd.Op1.value);
		break;
	  case 6: // opcode: 15-10 / disp25: 9-0, 31-16
		stmp = ua_next_byte();
		stmp |= ua_next_byte() << 8;
		cmd.Op1.addr = (short)stmp;
		cmd.Op1.phrase = op_hword&0x1f + rR0;
		cmd.Op1.type = o_displ;
		cmd.Op1.dtyp = dt_word;
		cmd.Op2.type = o_reg;
		cmd.Op2.reg = (op_hword>>5)&0x1f + rR0;
		if(cmd.itype==V810_ST_H || cmd.itype == V810_ST_W || cmd.itype == V810_ST_B) {
		  cmd.Op3=cmd.Op1;
		  cmd.Op1=cmd.Op2;
		  cmd.Op2=cmd.Op3;
		  cmd.Op3.type=o_void;
		}
		//msg("op3=%d, op1=%d, stmp=%hd", op_hword&0x1f, (op_hword>>6)&0x1f, stmp);
		break;
	  case 7: // opcode: 15-10 / disp25: 9-0, 31-16
		op_word = ua_next_byte() << 16;
		op_word |= ua_next_byte() << 24;
		op_word |= op_hword;
		switch((op_word >> 26)&0x3F) {
			
		  case 0x00: cmd.itype = V810_CMPF_S; break; 
		  case 0x02: cmd.itype = V810_CVT_WS; break;
		  case 0x03: cmd.itype = V810_CVT_SW; break;
		  case 0x04: cmd.itype = V810_ADDF_S; break;
		  case 0x05: cmd.itype = V810_SUBF_S; break;
		  case 0x06: cmd.itype = V810_MULF_S; break;
		  case 0x07: cmd.itype = V810_DIVF_S; break;
		  case 0x08: 
			if (is_vboy == 0)	return 0;
			cmd.itype = V810_XB; 
			break;
		  case 0x09: 
			if (is_vboy == 0)	return 0;
			cmd.itype = V810_XH; 
			break;
		  case 0x0A:
			if (is_vboy == 0)	return 0;
			cmd.itype = V810_REV;
			break;
		  case 0x0B: cmd.itype = V810_TRNC_SW; break;
		  case 0x0C:
			if (is_vboy == 0)	return 0; 
			cmd.itype = V810_MPYHW; 
			break;
		  case 0x01:
		  default:
			msg("%x:Unknown special opcode %x (num=%x)\n", cmd.ea, op_word, op_word >> 26);
			return 0;
		}
	    

		if (cmd.itype == V810_XB || cmd.itype == V810_XB)
		{
			cmd.Op1.type = o_reg;
			cmd.Op1.reg = (op_word >> 5) & 0x1f;
		}
		else
		{
			cmd.Op1.type = o_reg;
			cmd.Op1.reg = op_word & 0x1f;

			cmd.Op2.type = o_reg;
			cmd.Op2.reg = (op_word >> 5) & 0x1f;
		}
		break;
	  case 9: break;
	  default:
		msg("unknown format %d\n", format);
	}
  
	//just a kind of magic
	postprocess();

	return cmd.size;
}
