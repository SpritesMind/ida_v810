/*
 *  Interactive disassembler (IDA).
 *  NEC V810 module
 *
 */


#include "v810.hpp"

instruc_t Instructions[] = {
  { "",       0                       },  // Unknown Operation

  { "mov", CF_USE1|CF_CHG2},
  { "add", CF_USE1|CF_USE2|CF_CHG2},
  { "sub", CF_USE1|CF_USE2|CF_CHG2},
  { "cmp", CF_USE1|CF_USE2},
  { "shl", CF_USE1|CF_USE2|CF_CHG2|CF_SHFT},
  { "shr", CF_USE1|CF_USE2|CF_CHG2|CF_SHFT},
  { "jmp", CF_USE1|CF_STOP|CF_JUMP },
  { "sar", CF_USE1|CF_USE2|CF_CHG2|CF_SHFT},

  { "mul", CF_USE1|CF_USE2|CF_CHG2},
  { "div", CF_USE1|CF_USE2|CF_CHG2},
  { "mulu", CF_USE1|CF_USE2|CF_CHG2},
  { "divu", CF_USE1|CF_USE2|CF_CHG2},
  { "or", CF_USE1|CF_USE2|CF_CHG2},
  { "and", CF_USE1|CF_USE2|CF_CHG2},
  { "xor", CF_USE1|CF_USE2|CF_CHG2},
  { "not", CF_USE1|CF_CHG2 },

//mov
//add
  { "setf", CF_USE1|CF_CHG2},
//cmp
//shl
//shr
  { "cli", 0 },
//sar

  { "trap", 0 },
  { "reti", CF_STOP },
  { "halt", CF_STOP },
//
  { "ldsr", CF_CHG1|CF_USE2 },
  { "stsr", CF_CHG2|CF_USE1 },
  { "sei", 0 },

  { "SCH0BSU", CF_USE1|CF_USE2|CF_CHG2},
  { "SCH0BSD", CF_USE1|CF_USE2|CF_CHG2},
  { "SCH1BSU", CF_USE1|CF_USE2|CF_CHG2},
  { "SCH1BSD", CF_USE1|CF_USE2|CF_CHG2},
  //invalid
  //invalid
  //invalid
  //invalid
  { "ORBSU", CF_USE1|CF_USE2|CF_CHG2},
  { "ANDBSU", CF_USE1|CF_USE2|CF_CHG2},
  { "XORBSU", CF_USE1|CF_USE2|CF_CHG2},
  { "MOVBSU", CF_USE1|CF_USE2|CF_CHG2},
  { "ORNBSU", CF_USE1|CF_USE2|CF_CHG2},
  { "ANDNBSU", CF_USE1|CF_USE2|CF_CHG2},
  { "XORNBSU", CF_USE1|CF_USE2|CF_CHG2},
  { "NOTBSU", CF_USE1|CF_USE2|CF_CHG2},

  
  { "bv", CF_USE1|CF_JUMP },
  { "bc", CF_USE1|CF_JUMP },
  { "bl", CF_USE1|CF_JUMP },
  { "be", CF_USE1|CF_JUMP },
  { "bz", CF_USE1|CF_JUMP },
  { "bnh", CF_USE1|CF_JUMP },
  { "bn", CF_USE1|CF_JUMP },
  { "br", CF_USE1|CF_JUMP },
  { "blt", CF_USE1|CF_JUMP },
  { "ble", CF_USE1|CF_JUMP },
  { "bnv", CF_USE1|CF_JUMP },
  { "bnc", CF_USE1|CF_JUMP },
  { "bnl", CF_USE1|CF_JUMP },
  { "bne", CF_USE1|CF_JUMP },
  { "bnz", CF_USE1|CF_JUMP },
  { "bh", CF_USE1|CF_JUMP },
  { "bp", CF_USE1|CF_JUMP },
  { "nop", 0 },
  { "bge", CF_USE1|CF_JUMP},
  { "bgt", CF_USE1|CF_JUMP},

  { "movea", CF_USE1|CF_USE2|CF_CHG3 },
  { "addi", CF_USE1|CF_USE2|CF_CHG3 },
  { "jr", CF_USE1|CF_STOP|CF_JUMP },
  { "jal", CF_USE1|CF_CALL|CF_JUMP },
  { "ori", CF_USE1|CF_USE2|CF_CHG3 },
  { "andi", CF_USE1|CF_USE2|CF_CHG3 },
  { "xori", CF_USE1|CF_USE2|CF_CHG3 },
  { "movhi", CF_USE1|CF_USE2|CF_CHG3 },

  { "ld.b",       CF_USE1|CF_USE2|CF_USE3|CF_CHG3 },  
  { "ld.h",       CF_USE1|CF_USE2|CF_CHG3 },  
//
  { "ld.w",       CF_USE1|CF_USE2|CF_CHG3 },  
  { "st.b",       CF_USE1|CF_CHG2|CF_USE3 },  
  { "st.h",       CF_USE1|CF_CHG2|CF_USE3 }, 
//
  { "st.w",       CF_USE1|CF_CHG2|CF_USE3 },  

  { "in.b",       CF_USE1|CF_USE2|CF_CHG3 },  
  { "in.h",       CF_USE1|CF_USE2|CF_CHG3 },  
  { "caxi", 0 },
  { "in.w",       CF_USE1|CF_USE2|CF_CHG3 },  
  { "out.b",      CF_USE1|CF_CHG2|CF_USE3 },  
  { "out.h",      CF_USE1|CF_CHG2|CF_USE3 },  
  //
  { "out.w",      CF_USE1|CF_CHG2|CF_USE3 },  

  
  { "CMPF_S", CF_USE1|CF_USE2|CF_CHG3 },
  //invalid
  { "CVT_WS", CF_USE1|CF_USE2|CF_CHG3 },
  { "CVT_SW", CF_USE1|CF_USE2|CF_CHG3 },
  { "ADDF_S", CF_USE1|CF_USE2|CF_CHG3 },
  { "SUBF_S", CF_USE1|CF_USE2|CF_CHG3 },
  { "MULF_S", CF_USE1|CF_USE2|CF_CHG3 },
  { "DIVF_S", CF_USE1|CF_USE2|CF_CHG3 },
  { "XB", CF_USE1|CF_USE2|CF_CHG3 },
  { "XH", CF_USE1|CF_USE2|CF_CHG3 },
  { "REV", CF_USE1|CF_USE2|CF_CHG3 },
  { "TRNC_SW", CF_USE1|CF_USE2|CF_CHG3 },
  { "MPYHW", CF_USE1|CF_USE2|CF_CHG3 }
};

#ifdef __BORLANDC__
#if sizeof(Instructions)/sizeof(Instructions[0]) != V810_last
#error          No match:  sizeof(InstrNames) !!!
#endif
#endif
