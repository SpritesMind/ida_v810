/*
 *  Interactive disassembler (IDA).
 *  NEC V810
 */

#ifndef __INSTRS_HPP
#define __INSTRS_HPP

extern instruc_t Instructions[];

enum nameNum {
  V810_null = 0,    // Unknown Operation
  V810_MOV,
  V810_ADD,
  V810_SUB,
  V810_CMP,
  V810_SHL,
  V810_SHR,
  V810_JMP,
  V810_SAR,

  V810_MUL,
  V810_DIV,
  V810_MULU,
  V810_DIVU,
  V810_OR,
  V810_AND,
  V810_XOR,
  V810_NOT,

  //move imm5
  //add imm5
  V810_SETF,
  //cmp imm
  //shl imm
  //shr imm
  V810_CLI,		//vboy only
  //sar imm

  V810_TRAP,
  V810_RETI,
  V810_HALT,
  //invalid
  V810_LDSR,
  V810_STSR,
  V810_SEI,		//vboy only

  //bitstring
  V810_SCH0BSU,		//Search Bit 0 Upward
  V810_SCH0BSD,		//Search Bit 0 Downward
  V810_SCH1BSU,		//Search Bit 1 Upward
  V810_SCH1BSD,		//Search Bit 1 Downward
  //invalid
  //invalid
  //invalid
  //invalid
  V810_ORBSU,		//Or Bit String Upward
  V810_ANDBSU,		//And Bit String Upward
  V810_XORBSU,		//Exclusive Or Bit String Upward
  V810_MOVBSU,		//Move Bit String Upward
  V810_ORNBSU,		//Or Not Bit String Upward
  V810_ANDNBSU,		//And Not Bit String Upward
  V810_XORNBSU,		//Exclusive Or Not Bit String Upward
  V810_NOTBSU,		//Not Bit String Upward

  
  V810_BV,
  V810_BC,V810_BL,
  V810_BE,V810_BZ,
  V810_BNH,
  V810_BN,
  V810_BR,
  V810_BLT,
  V810_BLE,
  V810_BNV,
  V810_BNC,V810_BNL,
  V810_BNE,V810_BNZ,
  V810_BH,
  V810_BP,
  V810_NOP,
  V810_BGE,
  V810_BGT,
  
  
  V810_MOVEA,
  V810_ADDI,
  V810_JR,
  V810_JAL,
  V810_ORI,
  V810_ANDI,
  V810_XORI,
  V810_MOVHI,
  
  V810_LD_B,	  // Load Byte
  V810_LD_H,	  // Load Halfword
  //invalid
  V810_LD_W,	  // Load Word
  V810_ST_B,	  // Store Byte
  V810_ST_H,	  // Store Halfword
  //invalid
  V810_ST_W,	  // Store Word

  V810_IN_B,	  // Input Byte
  V810_IN_H,	  // Input Halfword
  V810_CAXI,
  V810_IN_W,	  // Input Word
  V810_OUT_B,	  // Output Byte
  V810_OUT_H,	  // Output Halfword
  //
  V810_OUT_W,	  // Output Word
  
  V810_CMPF_S,
  //invalid
  V810_CVT_WS,
  V810_CVT_SW,
  V810_ADDF_S,
  V810_SUBF_S,
  V810_MULF_S,
  V810_DIVF_S,
  V810_XB,			//VirtualBoy
  V810_XH,			//VirtualBoy
  V810_REV,			//VirtualBoy
  V810_TRNC_SW,
  V810_MPYHW,		//VirtualBoy
  /*
  V810_MAX3,
  V810_MUL3,
  V810_MAC3,
  V810_MULI,
  V810_MACI,
  V810_MULT3,
  V810_MACT3,
  V810_SATADD3,
  V810_SATSUB3,
  V810_SHLD3,
  V810_SHRD3,
  V810_BRKRET,
  V810_STBY,
  */
  
  V810_last
};

#endif
