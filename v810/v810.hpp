/*
 *  Interactive disassembler (IDA).
 *  NEC V810 module
 */

#ifndef _V810_HPP
#define _V810_HPP

#include "../idaidp.hpp"
#include "ins.hpp"

#include <list>
#include <pro.h>
#include <fpro.h>
#include <area.hpp>
#include <idd.hpp>
#include <ida.hpp>
#include <algorithm>
#include <name.hpp>
#include <idp.hpp>
#include <ieee.h>


//------------------------------------------------------------------------
// customization of cmd structure:

#define o_ind_mem   o_idpspec0      // @intmem
#define o_ind_reg   o_idpspec1      // @Rx

extern ea_t intmem;

//------------------------------------------------------------------------

enum V810_registers
{
  rR0,  rR1,  rR2,   rR3,   rR4,   rR5,   rR6,   rR7,
  rR8,  rR9,  rR10,  rR11,  rR12,  rR13,  rR14,  rR15,
  rR16,  rR17,  rR18,   rR19,   rR20,   rR21,   rR22,   rR23,
  rR24,  rR25,  rR26,  rR27,  rR28,  rR29,  rR30,  rR31,
  srR0,  srR1,  srR2,   srR3,   srR4,   srR5,   srR6,   srR7,
  srR8,  srR9,  srR10,  srR11,  srR12,  srR13,  srR14,  srR15,
  srR16,  srR17,  srR18,   srR19,   srR20,   srR21,   srR22,   srR23,
  srR24,  srR25,  srR26,  srR27,  srR28,  srR29,  srR30,  srR31,
  rVcs, rVds,
  rLastRegister
};

typedef struct
{
  uchar addr;
  char *name;
  char *cmt;
} predefined_t;

extern predefined_t iregs[];

extern bool is_vboy;

//------------------------------------------------------------------------


// prototypes -- out.cpp
void idaapi header( void );
void idaapi footer( void );
void idaapi segstart( ea_t ea );
void idaapi segend( ea_t ea );
void idaapi out( void );
bool idaapi outop( op_t &op );

// prototypes -- ana.cpp
int  idaapi ana( void );

// prototypes -- emu.cpp
int  idaapi emu( void );

void idaapi V810_data( ea_t ea );

#endif
