/*
 *  Interactive disassembler (IDA).
 *  NEC V810 module
 */

#include "v810.hpp"

//--------------------------------------------------------------------------

ea_t intmem = 0;

//--------------------------------------------------------------------------

const char *RegNames[rLastRegister] =
{
  "r0",  "r1",  "r2",   "sp",   "gp",   "r5",   "r6",   "r7",
  "r8",  "r9",  "r10",  "r11",  "r12",  "r13",  "r14",  "r15",
  "r16", "r17", "r18",  "r19",  "r20",  "r21",  "r22",  "r23",
  "r24", "r25", "r26", "r27", "r28", "r29", "r30", "lr",
  //see LDSR and STSR
  "eipc", "eipsw", "fepc", "fepsw", "ecr", "psw", "pir", "tkcw",
  //"dpc", "dpsw", "invalid", "invalid", "invalid", "invalid", "invalid", "hccw",
  "<Unk>","<Unk>","<Unk>","<Unk>","<Unk>","<Unk>","<Unk>","<Unk>",
  "<Unk>","<Unk>","<Unk>","<Unk>","<Unk>","<Unk>","<Unk>","<Unk>",
  "chcw","adtre","<Unk>","<Unk>","<Unk>","<Unk>","<Unk>","<Unk>",
  "cs",  "ds"
};

//----------------------------------------------------------------------

typedef struct
{
  int off;
  char *name;
  char *cmt;
} entry_t;

static entry_t entries[] =
{
  {  0, "irq0", "DAV0, IRQ0, Comparator" },
  {  2, "irq1", "DAV1, IRQ1" },
  {  4, "irq2", "DAV2, IRQ2, TIN, Comparator" },
  {  6, "irq3", "IRQ3, Serial in" },
  {  8, "irq4", "T0, Serial out" },
  { 10, "irq5", "T1" },
};

//----------------------------------------------------------------------
/*
static ulong specialSeg( sel_t sel )
{
  segment_t *s = get_segm_by_sel( sel );

  if( s != NULL )
  {
    if( s->type != SEG_IMEM )
    {
      s->type = SEG_IMEM;
      s->update();
    }

    return s->startEA;
  }

  return BADADDR;
}*/

//--------------------------------------------------------------------------

static int notify(processor_t::idp_notify msgid, ...) { // Various messages:
  va_list va;
  va_start(va, msgid);

// A well behaving processor module should call invoke_callbacks()
// in his notify() function. If this function returns 0, then
// the processor module should process the notification itself
// Otherwise the code should be returned to the caller:

  int code = invoke_callbacks(HT_IDP, msgid, va);
  if ( code ) return code;


  switch(msgid)
  {
    case processor_t::init:
      inf.mf = 0;                                 // MSB first
      break;

	case processor_t::newprc:
    {
      int procnum = va_arg(va, int);
      is_vboy = (procnum == 1); //0: V810, 1: V810 vboy
      break;
    }
    default:
      break;
  }
  va_end(va);

  return(1);
}

//--------------------------------------------------------------------------

static asm_t V810asm = {
  AS_COLON,
  0,
  "NEC V810 assembler",
  0,
  NULL,
  NULL,
  ".org",
  ".end",

  ";",          // comment string
  '\'',         // string delimiter
  '\0',         // char delimiter (no char consts)
  "\\\"'",      // special symbols in char and string constants

  ".ascii",     // ascii string directive
  ".byte",      // byte directive
  ".word",      // word directive
  ".dword",         // dword  (4 bytes)
  NULL,         // qword  (8 bytes)
  NULL,         // oword  (16 bytes)
  NULL,         // float  (4 bytes)
  NULL,         // double (8 bytes)
  NULL,         // tbyte  (10/12 bytes)
  NULL,         // packed decimal real
  NULL,         // arrays (#h,#d,#v,#s(...)
  ".block %s",  // uninited arrays
  ".equ",       // Equ
  NULL,         // seg prefix
//  preline, NULL, operdim,
  NULL, NULL, NULL,
  NULL,
  "$",
  NULL,		// func_header
  NULL,		// func_footer
  NULL,		// public
  NULL,		// weak
  NULL,		// extrn
  NULL,		// comm
  NULL,		// get_type_name
  NULL,		// align
  '(', ')',	// lbrace, rbrace
  NULL,    // mod
  NULL,    // and
  NULL,    // or
  NULL,    // xor
  NULL,    // not
  NULL,    // shl
  NULL,    // shr
  NULL,    // sizeof
};

static asm_t *asms[] = { &V810asm, NULL };

//--------------------------------------------------------------------------

static const char *shnames[] = 
{ 
	"V810", 
	"VirtualBoy",
	NULL 
};
static const char *lnames[]  = 
{ 
	"NEC V810", 
	"Nintendo V810",
	NULL 
};

//--------------------------------------------------------------------------
/*
static uchar 
    startcode1[] = { 0x63, 0xa4 },   // addi #xxxh, sp, sp
	startcode2[] = { 0x74, 0x44 },   // add #-C, sp
	startcode3[] = { 0x78, 0x44 },   // add #-8, sp
	startcode4[] = { 0x7C, 0x44 };   // add #-4, sp
	
static bytes_t startcodes[] = {
  { sizeof( startcode1 ),  startcode1 },
  { sizeof( startcode2 ),  startcode2 },
  { sizeof( startcode3 ),  startcode3 },
  { sizeof( startcode4 ),  startcode4 },
  { 0, NULL }
};

static uchar retcode[]  = { 0x1f, 0x18 };   // ret

static bytes_t retcodes[] = {
  { sizeof( retcode ),  retcode },
  { 0, NULL }
};
*/

//-----------------------------------------------------------------------
//      Processor Definition
//-----------------------------------------------------------------------
processor_t LPH =
{
  IDP_INTERFACE_VERSION,        // version
  0x8000+0x810,                      // id
  /*PR_SEGS |*/ PR_DEFSEG32 | PR_USE32 | PRN_HEX | PR_RNAMESOK | PR_NO_SEGMOVE, // flags
  //PRN_HEX|PR_BINMEM,
  8,                            // 8 bits in a byte for code segments
  8,                            // 8 bits in a byte for other segments

  shnames,    // short processor names (null term)
  lnames,     // long processor names (null term)

  asms,       // array of enabled assemblers

  notify,     // Various messages:

  header,     // produce start of text file
  footer,     // produce end of text file

  segstart,   // produce start of segment
  segend,     // produce end of segment

  NULL,			// function to produce assume directives

  ana,			// Analyze one instruction and fill 'cmd' structure.
  emu,			// Emulate instruction
  out,			// Generate text representation of an instruction in 'cmd' structure
  outop,		// Generate text representation of an instructon operand.
  intel_data,    // Generate text represenation of data items
  NULL,       // compare operands
  NULL,       // can have type

  qnumber(RegNames),    // Number of registers
  RegNames,             // Register names
  NULL,                 // get abstract register

  0,                    // Number of register files
  NULL,                 // Register file names
  NULL,                 // Register descriptions
  NULL,                 // Pointer to CPU registers

  rVcs,					// number of first segment register
  rVds,					// number of last segment register
  0,                    // size of a segment register
  rVcs,					// ?
  rVds,					// ?

  NULL, //startcodes,   // No known code start sequences
  NULL, //retcodes,		// Array of 'return' instruction opcodes

  0, 
  V810_last,
  Instructions,	

  NULL,                 // int  (*is_far_jump)(int icode);
  NULL,                 // Translation function for offsets
  0,                    // int tbyte_size;  -- doesn't exist
  NULL,                 // int (*realcvt)(void *m, ushort *e, ushort swt);
  { 0, 0, 0, 0 },       // char real_width[4];
                        // number of symbols after decimal point
                        // 2byte float (0-does not exist)
                        // normal float
                        // normal double
                        // long double
  NULL,                 // int (*is_switch)(switch_info_t *si);
  NULL,                 // long (*gen_map_file)(FILE *fp);
  NULL,                 // ulong (*extract_address)(ulong ea,const char *string,int x);
  NULL,                 // int (*is_sp_based)(op_t &x); -- always, so leave it NULL
  NULL,                 // int (*create_func_frame)(func_t *pfn);
  NULL,                 // int (*get_frame_retsize(func_t *pfn)
  NULL,                 // void (*gen_stkvar_def)(char *buf,const member_t *mptr,long v);
  gen_spcdef,           // Generate text representation of an item in a special segment
};
