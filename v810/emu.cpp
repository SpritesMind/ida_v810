/*
 *  Interactive disassembler (IDA).
 *  NEC V810 module
 */

#include "v810.hpp"

static int flow;

// handle immediate values
inline void handle_imm(void) {
  doImmd(cmd.ea);
}

// emulate operand
static void handle_operand(op_t &op, bool loading) {
  if(is_forced_operand(cmd.ea, op.n)) return;
//    msg("handle_operation(addr=%x, type=%d, value=%x)\n", op.addr, op.type, op.value);  
  switch (op.type) {
  case o_near:  // Address
    // branch label - create code reference (call or jump according to the instruction)
    if (cmd.get_canon_feature() & CF_JUMP) { 
      ea_t ea = toEA(cmd.cs, op.addr);
      cref_t ftype = fl_JN;
      if ( cmd.itype == V810_JAL ) {
	if ( !func_does_return(ea) )
	  flow = false;
	ftype = fl_CN;
      }
      if(ea > 0x8b50000) ua_add_dref(op.offb, ea, dr_R);
      else ua_add_cref(op.offb, ea, ftype);
    }
    break;
  case o_imm: // Immediate
//			msg("o_imm: value=%x", op.value);
    if (!loading) msg("handle_operand(): o_imm with CF_CHG\n");
    handle_imm();
//			msg("o_imm2: value=%x\n", op.value);
    if (cmd.itype == V810_MOVEA) {
//				op.value = toEA(cmd.cs, get_reg_value(cmd.Op2.reg, 10));
//				op_offset(cmd.ea, 2, REF_OFF32, v, v & 0xFFFF0000);
// if the value was converted to an offset, then create a data xref:
//			msg("o_imm3: value=%x\n", op.value);
      handle_imm();
//			msg("o_imm4: value=%x\n", op.value);
    }
    if (isOff(uFlag, op.n)) ua_add_off_drefs(op, dr_O);
    break;

    // Displ
  case o_displ:
    handle_imm();
    // if the value was converted to an offset, then create a data xref:
    if (isOff(uFlag, op.n)) {
//				msg("ua_add_off_drefs(%x)\n", cmd.ea);
      ua_add_off_drefs(op, loading ? dr_R : dr_W);
    }
    // create stack variables if required
    if (may_create_stkvars() && !isDefArg(uFlag, op.n)) {
      func_t *pfn = get_func(cmd.ea);
      if (pfn != NULL && (op.reg == rR3) && pfn->flags & FUNC_FRAME) {
	if (ua_stkvar2(op, op.addr, STKVAR_VALID_SIZE)) {
	  op_stkvar(cmd.ea, op.n);
	}
      }
    }
    break;
    
  case o_phrase:
    /* create stack variables if required */
    if (may_create_stkvars() && !isDefArg(uFlag, op.n)) {
      func_t *pfn = get_func(cmd.ea);
      if (pfn != NULL && (op.reg == rR3) && pfn->flags & FUNC_FRAME) {
	if (ua_stkvar2(op, 0, STKVAR_VALID_SIZE)) {
	  op_stkvar(cmd.ea, op.n);
	}
      }
    }
    break;
    
    // Phrase - register - void : do nothing
  case o_reg:
  case o_void:
    break;
    
    // Others types should never be called
  default:
    msg("handle_operand(): unknown type %d", op.type);
    break;
  }
}

/* Jump table examples:
loc_89F204A:                            ; CODE XREF: ROM:089F2044
                 shl     #2, r10
                 movhi   #89Fh, r0, r30
                 add     r30, r10
                 ld.w    2058h(r10), r30
                 jmp     r30
; ---------------------------------------------------------------------------
                .dword loc_89F2068
                .dword loc_89F20A2
   As far as I can tell, any computed JMP that is not to LR, and is not
   immediately preceded by a MOVEA statement is immediately followed by a jumptable.
 */
void find_jumptable(void) {
  insn_t saved = cmd;
  int table_len = 0, table_start;
  if (cmd.Op1.reg == rR31) return;  // lr is never a jump table
  if (decode_prev_insn(cmd.ea) == BADADDR) {
    msg("ERROR: find_jumptable couldn't find instruction previous to %x\n", saved.ea);
    goto done;
  }
  if (cmd.itype == V810_MOVEA) goto done;
  cmd = saved;
  table_start = cmd.ea + 2;
  if(get_word(table_start)==0) table_start += 2;
  while (abs(get_long(table_start + table_len*4) - table_start) < 10000) {
    ea_t ea = table_start + table_len*4;
    doDwrd(ea, 4);
    op_offset(ea, 0, REF_OFF32);
//		msg("1ua_add_cref(%x)\n", get_long(ea));
    ua_add_cref(0, get_long(ea), fl_JN);
    table_len++;
  }
 done:
  cmd = saved;
  return;
}

//----------------------------------------------------------------------

int idaapi emu( void ) {
  uint32 Feature = cmd.get_canon_feature();
  //insn_t saved = cmd;
  char buf[1024];
  
  flow = ((Feature & CF_STOP) == 0);

  if (Feature & CF_USE1 )   handle_operand( cmd.Op1, 1 );
  if (Feature & CF_USE2 )   handle_operand( cmd.Op2, 1 );
  if (Feature & CF_USE3 )   handle_operand( cmd.Op3, 1 );

  if (Feature & CF_JUMP )   QueueMark( Q_jumps, cmd.ea );

  if (Feature & CF_CHG1 )   handle_operand( cmd.Op1, 0 );
  if (Feature & CF_CHG2 )   handle_operand( cmd.Op2, 0 );
  if (Feature & CF_CHG3 )   handle_operand( cmd.Op3, 0 );

  if (flow) {
    ua_add_cref( 0, toEA(cmd.cs, cmd.ea+cmd.size), fl_F );
//	msg("2ua_add_cref(%x)\n", cmd.ea+ cmd.size);
  }
  
  switch(cmd.itype) {
  case V810_ST_H:
  case V810_ST_W:
  case V810_ST_B:
//		ua_add_dref(2, toEA(cmd.cs, cmd.Op2.addr+get_reg_value(cmd.Op2.phrase, 10)), dr_W);
    break;
  case V810_LD_H:
    doWord(cmd.Op1.addr, 2); 
    ua_add_dref(2, cmd.Op1.addr, dr_R);
    break;
  case V810_LD_W:
    doDwrd(cmd.Op1.addr, 4);
    ua_add_dref(2, cmd.Op1.addr, dr_R);
    break;
  case V810_LD_B:
    doByte(cmd.Op1.addr, 1);
    ua_add_dref(2, cmd.Op1.addr, dr_R);
//		ua_add_dref(2, toEA(cmd.cs, cmd.Op1.addr+get_reg_value(cmd.Op1.phrase, 10)), dr_R);
    break;
  case V810_MOVEA:
    if (cmd.Op2.reg == rR31) {
      ua_add_cref(2, cmd.Op1.addr, fl_F);
//			msg("3ua_add_cref(%x)\n", cmd.Op1.addr);
    }
    else ua_add_dref(2, cmd.Op1.addr, dr_R);
    break;
  case V810_JMP:
    find_jumptable();
    break;
  default: break;
  }
  //  cmd = saved;
  return 1;
}
